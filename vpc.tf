locals {
  subnets = {
    subnet-a = {
      zone = "ru-central1-a"
      cidr = ["192.168.10.0/24"]
    }
    subnet-b = {
      zone = "ru-central1-b"
      cidr = ["192.168.11.0/24"]
    }
    subnet-c = {
      zone = "ru-central1-c"
      cidr = ["192.168.12.0/24"]
    }
  }
}

resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-foreach" {
  for_each        = local.subnets
  name            = each.key
  zone            = each.value.zone
  network_id      = yandex_vpc_network.network-1.id
  v4_cidr_blocks  = each.value.cidr
}
